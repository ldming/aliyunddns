﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AliyunDDNS.Core
{
    public static class DdnsHelper
    {
        public static string ComposeToken(string token,string tokenKey) {
            return Md5Helper.GetMd5(token + tokenKey, outLength: 16);
        }

        public static bool VerifyToken(string token1,string token2) {
            return token1 == token2;
        }
    }
}
