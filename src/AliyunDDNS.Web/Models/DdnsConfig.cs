﻿using System.Collections.Generic;

namespace AliyunDDNS.Web.Models
{
    public class Config
    {
        /// <summary>
        /// RegionId
        /// </summary>
        public string RegionId { get; set; }

        /// <summary>
        /// Period / 调用 callback 的时间间隔（以毫秒为单位）
        /// </summary>
        public int IntervalMillisecond { get; set; }

        /// <summary>
        /// PageSize
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// AccessKeyId
        /// </summary>
        public string AccessKeyId { get; set; }

        /// <summary>
        /// AccessKeySecret
        /// </summary>
        public string AccessKeySecret { get; set; }
    }

    public class DomainRecord
    {
        /// <summary>
        /// DomainName
        /// </summary>
        public string DomainName { get; set; }

        /// <summary>
        /// RR
        /// </summary>
        public string RR { get; set; }

        /// <summary>
        /// PageSize
        /// </summary>
        public int PageSize { get; set; }
    }

    public class DdnsConfig
    {
        /// <summary>
        /// Config
        /// </summary>
        public Config Config { get; set; }

        /// <summary>
        /// DomainRecords
        /// </summary>
        public List<DomainRecord> DomainRecords { get; set; }
    }
}