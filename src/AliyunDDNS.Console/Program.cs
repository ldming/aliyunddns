﻿using AliyunDDNS.Core;
using System;

namespace AliyunDDNS.Consoles
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");

            var content = "我是测试内容，用来测试加密解密，欧耶";
            var key = "keysseeekeysseee";
            Console.WriteLine(content);

            var cipherText = AESHelper.Encrypt(content, key);
            Console.WriteLine(cipherText);

            var clearText = AESHelper.Decrypt(cipherText, key);
            Console.WriteLine(clearText);

            content = "Admin";

            var ss = Md5Helper.GetMd5(content,true);
            Console.WriteLine(ss);

            var sss = Md5Helper.GetMd5(content,false);
            Console.WriteLine(sss);

            var ssss = Md5Helper.GetMd5(content,outLower:true);
            Console.WriteLine(ssss);

            var sssss = Md5Helper.GetMd5(content, outLower:false);
            Console.WriteLine(sssss);

            var ssssss = Md5Helper.GetMd5(content,0,true);
            Console.WriteLine(ssssss);

            var sssssss = Md5Helper.GetMd5(content, 0, false);
            Console.WriteLine(sssssss);

            //var sss = CryptographyHelper.Md52(content);
            //Console.WriteLine(sss);

            Console.Read();
        }
    }
}
