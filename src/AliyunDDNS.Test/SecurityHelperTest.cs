using AliyunDDNS.Core;
using System;
using Xunit;

namespace AliyunDDNS.Test
{
    public class SecurityHelperTest
    {
        [Fact]
        public void Test1()
        {
            var content = "���ǲ������ݣ��������Լ��ܽ��ܣ�ŷҮ";
            var key = "keydesesefeseftf";
            var cipherText = AESHelper.Encrypt(content,key);

            var clearText = AESHelper.Decrypt(cipherText, key);

            Assert.Equal(content, clearText);
        }
    }
}
